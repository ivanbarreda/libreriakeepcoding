//
//  Library.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 25/9/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//


import Foundation

typealias Books = Multidictionary<Tag,Book>

class Library {
    
    
    // MARK: - Properties
    var books: Books
    
    private func generateLibrary(withBooks books: [Book]){
    
        //Generar el indice
        for book in books {
            for tag in book.tags {
                
                insert(value: book, forTag: tag)
                
            }
        }
    }
    
    
    
    // MARK: - Initializatcion
    init(withModel model: [Book]){
        
        books = Books()
        generateLibrary(withBooks: model)
    }
    
    
    func insert(value book: Book,forTag tag: Tag){
        self.books.insert(value: book, forTag: tag)

    }
    
    
    func remove(value book: Book, forTag tag: Tag){
        
        self.books.remove(value: book, forTag: tag)
        
    }
    
    

    
}





// MARK: - DataSource
extension Library{
    
    func countTag(tag: Tag){
        
        print (self.books.countForKey(tag: tag))
        
    }
    
    
    var count: Int {
        get{
            return self.books.count
        }
    }
    
    var tags:[Tag]{
    
            return books.keys.sorted()
    }
    
    func booksCount(atSection section: Tag)->Int{
        
        
        return books.countForKey(tag: section)
        
    }
    
    func book(atIndexPath indexPath: IndexPath)->Book{
                
        let tag = books.keys.sorted()[indexPath.section]
       
        let b = books.values(forKey: tag)?.sorted()
        

        return b![indexPath.row]
        
    }
    
    func contain(book: Book, inTag tag: Tag)->Bool{
        

        
        guard let a = books.values(forKey: tag)?.sorted(), a.contains(book) else{
            
            return false
        }

        
        return true
        
//        if books.values(forKey: tag))?.sorted().contains(book) {
//            
//            print("Contiene el libro \(book.titulo)")
//            return true
//        }
//        
        
        
    }
    
}








