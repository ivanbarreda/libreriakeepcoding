//
//  PDFViewController.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 3/10/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import UIKit

class PDFViewController: UIViewController {

    @IBOutlet weak var pdfViewer: UIWebView!
    
    let model: Book
    
    
    init(withModel model: Book) {
        
        self.model = model
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        

        //TODO: Rebvisar la baseURL
        
        pdfViewer.load(model.pdf, mimeType: "application/pdf", textEncodingName: "utf-8", baseURL: URL(string: "http://google.es")!)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
