//
//  AppDelegate.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 24/9/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
        do{
            let json = try downloadJSON()
            var libros = [Book]()
            
            
            for item in json {
                
                
                if let libro = decode(bookItem: item as! NSDictionary) {
                    libros.append(libro)
                    
                }else{
                    
                    print("Error al descargar libro")
                }
                
            }
            
            
            let library = Library(withModel: libros)
            
            
            //Creamos el Library VC
            let lVC = LibraryTableViewController(withLibrary: library)
            let bVC = BookViewController(withModel:libros[5])
            
            let lNC = UINavigationController(rootViewController: lVC)
            let bNC = UINavigationController(rootViewController: bVC)
            
            
            
            //Generamos el navigation VC
            //let sNav = UINavigationController(rootViewController: lVC)
            
            let sNav = UISplitViewController()
            sNav.viewControllers = [lNC, bNC]
            
            
            window?.rootViewController = sNav
            window?.makeKeyAndVisible()
            
            
        }catch{
            fatalError("Algo no ha ido bien en la descarga")
        }
        
        print("Finalizada la descarga")
        
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - Test
    
    func testSet(){
        
        
        
        
        
    }
    
    
}

