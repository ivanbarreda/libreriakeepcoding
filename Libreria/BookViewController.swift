//
//  BookViewController.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 27/9/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import UIKit

class BookViewController: UIViewController {
    
    var book: Book
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var autores: UILabel!
    @IBOutlet weak var tags: UILabel!
    @IBOutlet weak var favorite: UIButton!
    @IBOutlet weak var read: UIButton!
    
    @IBOutlet weak var portada: UIImageView!
    
    @IBAction func leerPDF(_ sender: AnyObject) {
        
        
        
        let PDFVC: PDFViewController = PDFViewController(withModel: book)
        self.navigationController?.pushViewController(PDFVC, animated: true)
        
    }
    
    @IBAction func favoriteTouchUpInside(_ sender: AnyObject) {
        
        print("Favorite Touch")
        
        book.changeFav()
        
        NotificationCenter.default.post(name: .favoriteDidChange, object: nil, userInfo: [favoriteTag: book])
        
        updateInterface()
    }
    // MARK: - LifeCylce
    
    init(withModel book: Book){
        
        self.book = book
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        baseUI()
        NotificationCenter.default.addObserver(self, selector: #selector(bookDidChange), name: NSNotification.Name(rawValue: BookDidChangeNotification), object: nil)
        
        
        updateInterface()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    

    // MARK: - Utilities
    func bookDidChange(_ notification: Notification){
        
        self.book = (notification as NSNotification).userInfo![BookKey] as! Book
        
        updateInterface()
    }
    
    
    func baseUI(){
        
        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9803921569, blue: 0.9333333333, alpha: 1)
        titulo.textColor = #colorLiteral(red: 0.1137254902, green: 0.2078431373, blue: 0.3411764706, alpha: 1)
        titulo.font = UIFont(name: titulo.font.fontName, size: 40)
        
        tags.textColor = #colorLiteral(red: 0.2705882353, green: 0.4823529412, blue: 0.6156862745, alpha: 1)
        autores.textColor = #colorLiteral(red: 0.2705882353, green: 0.4823529412, blue: 0.6156862745, alpha: 1)

        
        favorite.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.2235294118, blue: 0.2745098039, alpha: 1)
        favorite.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: [])
        
        favorite.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.2235294118, blue: 0.2745098039, alpha: 1)
        favorite.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: [])
        
        read.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.2235294118, blue: 0.2745098039, alpha: 1)
        read.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: [])
    }
    
    
    func updateInterface(){
        
        
        title = book.titulo
        
        titulo.text = book.titulo
        autores.text = book.autores
        
        read.setTitle("Read \(book.titulo)", for: [])
        
        var tagsA = [String]()
        
        for tag in book.tags.sorted(){
            tagsA.append(tag.name)
        }

        tags.text = tagsA.joined(separator: ", ")
        
        let titleLabel = book.favorite == true ? "👎 Remove Favorite" : "👍 Add Favorite"
        
        favorite.setTitle(titleLabel, for: .normal)
        portada?.image = book.portada
        
    }

    
}
