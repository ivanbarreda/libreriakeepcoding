//
//  JSON.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 24/9/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import Foundation
import UIKit


func downloadJSON() throws ->NSArray{
    
    guard let urlRemoteLibrary = URL(string: "https://t.co/K9ziV0z3SJ"), let jsonData = try? Data(contentsOf: urlRemoteLibrary) else{
        fatalError("No se ha podido descargar")
        
    }
    return try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! NSArray
    
    
}

func decode(bookItem item: NSDictionary)->Book?{
    
    
    do{
        
        return try validateBook(item)
        
    }catch{
        
        
        return nil
        
    }
    
    
}




private func saveDataInDocumentDirectory(url: String)->Data?{
    

    let fileNane = URL(fileURLWithPath: url).lastPathComponent
    
    let rutaDirectorio = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(fileNane)


    
    if FileManager.default.fileExists(atPath: rutaDirectorio.path){
        //Si existe lo devolvemos
        return try? Data(contentsOf: URL(fileURLWithPath: rutaDirectorio.path))
    }
    
    //Comprobamos que el Data remoto existe
    guard  let docURL = URL(string: url), let docData = try? Data(contentsOf: docURL) else {
        return nil
    }
    

    do{
        try docData.write(to: rutaDirectorio)
        return docData

    }catch{
        
        return nil

        
    }
    
    
}

private func validateBook(_ item: NSDictionary) throws->Book{

    
    guard let image_url = item["image_url"] as? String, let portadaData = saveDataInDocumentDirectory(url: image_url), let portadaImage = UIImage(data: portadaData) else{
        
        print("Error imagen de \(item)")
        throw JSONErrror.errorDownloadingData
    }
    
    
    
    //Demo
    guard let pdf_url = item["pdf_url"] as? String, let pdfData = saveDataInDocumentDirectory(url: pdf_url) else{
    
    //guard let pdf_url = item["image_url"] as? String, let pdfURL = URL(string: pdf_url), let pdfData = try? Data(contentsOf: pdfURL) else{
        
        print("Error pdf de \(item)")
        
        throw JSONErrror.errorDownloadingData
    }
    
    
    guard  let titulo = item["title"] as? Titulo else{
        print("Error al codificar \(item)")
        
        throw JSONErrror.errorDownloadingData
        
        
    }
    
    guard let autores = item["authors"] as? Autores else{
        print("Error al codificar AUTHORS \(item)")
        throw JSONErrror.errorDownloadingData
        
    }
    
    guard let tags = item["tags"] as? String else {
        
        print("Error al codificar TAGS \(item)")
        throw JSONErrror.errorDownloadingData
        
    }
    
    

    var tt: Tags = Tags()
    for tagString in tags.components(separatedBy: ", ") {
        
        tt.insert(Tag(name: tagString)
)
        
    }
    //let tagsArray = tags.components(separatedBy: ", ")
    
    
    //TODO: Insert Tags
//    for tag in  tags.components(separatedBy: ", ") {
//     
//        tt.insert(tag)
//    }
//    
    
    return Book(titulo: titulo, portada: portadaImage, autores: autores, pdf: pdfData as PDF, tags: tt)
    
}








