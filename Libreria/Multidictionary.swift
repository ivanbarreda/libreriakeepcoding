//
//  Multidictionary.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 4/10/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import Foundation


struct Multidictionary<Key: Hashable, Value: Hashable>{
    
    
    typealias Bucket = Set<Value>
    
    var dict: [Key: Bucket]
    
    init(){
        
        dict = Dictionary()
        
        
        
    }
    
    func countForKey(tag: Key)->Int{
        
        return (dict[tag]?.count)!
    }
    
    func values(forKey key: Key)->Bucket?{
        
        return dict[key]
    }
    
    
    var count: Int {
        get{
            return dict.count
        }
    }
    
    var keys : LazyMapCollection<Dictionary<Key, Bucket>,Key> {
        return dict.keys
    }
    
    var values :LazyMapCollection<Dictionary<Key, Bucket>,Bucket> {
        return dict.values
    }

    
}


//Add And Remove Values
extension Multidictionary{
    
    mutating func insert(value: Value,forTag tag: Key){
        

        if var existingValue = self.dict[tag]{
            
            existingValue.insert(value)
            dict[tag] = existingValue
        }else{
            
             dict[tag] = [value]
        }
        

        
    }
    
    mutating func remove(value: Value,forTag tag: Key){
        
        if dict[tag] == nil{
            return
        }

        //Comprabamos is existe el tag
        guard var contenido = dict[tag] else{
            return
        }
        
        //Comprobar si el contenedor tiene el elemento
        if !contenido.contains(value){
            return
        }
        
       contenido.remove(value)
        
        if contenido.isEmpty{
            dict.removeValue(forKey: tag)
        }else{
            
            dict[tag] = contenido
        }
    }
    
    
    
    
}
