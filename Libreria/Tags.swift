//
//  Tag.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 4/10/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import Foundation

typealias Tags = Set<Tag>
typealias TagName = String


let favoriteTag = "Favorite"


class Tag {
    
    var name: TagName
    
    init(name: TagName){
        
        
        self.name = name.capitalized(with: Locale.current)
    }
    
}

class TagFavorite: Tag {
    
    init(){
        
        super.init(name: favoriteTag)
        
    }
    
}

//Extensions
extension Tag: Hashable{
    
    public var hashValue: Int {
        
        get {
            
            return name.hashValue
        }
    }

}


extension Tag: Equatable{
    
    public static func ==(lhs: Tag, rhs: Tag) -> Bool{
        
        return lhs.hashValue == rhs.hashValue
    }
    
}

extension Tag: Comparable{
    
    var proxyForComparation: String {
        
        get{
            
            
            if name == favoriteTag{
                return "aaa\(name)"
            }
            return "zzz\(name)"
            
        }
    }
    
    public static func <(lhs: Tag, rhs: Tag) -> Bool{
        return lhs.proxyForComparation < rhs.proxyForComparation
    }
    


}



