//
//  JSONError.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 3/10/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import Foundation


enum JSONErrror: Error {
    
    case errorDownloadingData
}
