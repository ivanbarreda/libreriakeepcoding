//
//  LibraryTableViewController.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 25/9/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import UIKit


let BookDidChangeNotification = "Select Book Did Change"
let BookKey = "BookChanged"

class LibraryTableViewController: UITableViewController {
    
    
    
    var model: Library
    
    init(withLibrary model: Library){
        
        self.model = model
        
        super.init(nibName: nil, bundle: nil)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(favoriteDidChange), name: Notification.Name("favoriteDidChange"), object: nil)
    }
    
    
    func favoriteDidChange(_ notification: Notification){
        
        
        guard let book = notification.userInfo?[favoriteTag] as? Book else {
            return
        }
    
        if book.favorite {
             model.insert(value: book, forTag: TagFavorite())
        }else{
            model.remove(value: book, forTag: TagFavorite())
        }
        
        self.tableView.reloadData()

        
    }
    
    
    
    // MARK: - Table view data source
    // Sections
    override func numberOfSections(in tableView: UITableView) -> Int {

        return model.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return  model.tags[section].name
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        
        //TODO: Refactorizar
        let nameSection = model.tags[section]
        return model.booksCount(atSection: nameSection)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Tipo de celda
        let cellID = "BookCell"
        
        
       let book = model.book(atIndexPath: indexPath)
        
        //Crear la celda
        
        //TODO: Crear 2 tipos de celda
        var cell = tableView.dequeueReusableCell(withIdentifier: cellID) //Puede no tener una celda, si usas del sistema
        //let cell = tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) //Si no esta te la crea, apropiado para celdas custom
        
        // Guard compueba si el Optional es  NIL, si es NIL entra, si no es Nil lo extrae y continua
        
        if cell == nil {
            cell = UITableViewCell(style: .subtitle,
                                   reuseIdentifier: cellID)
        }

        //Sincronizar personaje -> celda
        
        cell?.detailTextLabel?.text = book.autores
        cell?.textLabel?.text = book.titulo
        cell?.imageView?.image = book.portada
        
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let book = model.book(atIndexPath: indexPath)
        
        //send notification
        let nc = NotificationCenter.default
        let notificacion = Notification(name: Notification.Name(rawValue: BookDidChangeNotification), object: self, userInfo: [BookKey: book] )
        nc.post(notificacion)
        
    }
    
}
