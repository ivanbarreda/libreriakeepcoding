//
//  Book.swift
//  Libreria
//
//  Created by Ivan Cuxaro on 24/9/16.
//  Copyright © 2016 Buzzapi. All rights reserved.
//

import Foundation
import UIKit


typealias Titulo = String

typealias Autor = String
//typealias Autores = [Autor]
typealias Autores = String


typealias Portada = UIImage

typealias PDF = Data




typealias Favorite = Bool
//typealias Tags = Tag





// Definition:
extension Notification.Name {
    static let favoriteDidChange = Notification.Name("favoriteDidChange")
}


class Book{
    
    
    var titulo: Titulo
    var portada: Portada
    var autores: Autores
    var pdf: PDF
    var tags: Tags
    var favorite : Favorite
    
    init(titulo: Titulo, portada: Portada, autores: Autores, pdf: PDF, tags: Tags){
        
        (self.titulo, self.portada, self.autores, self.pdf, self.tags, self.favorite) = (titulo, portada, autores, pdf, tags, false)
    }
    

}


extension Book{
    
    
    func changeFav(){
  
        favorite = favorite == true ? false : true
        
        
    }
    

}


extension Book: Hashable{
    
    
    var proxyForHash: String{
        get{
            return "\(titulo)\(autores)"
        }
    }
    
    public var hashValue: Int {
        get {
            
            return proxyForHash.hashValue
            
        }
    }

}

extension Book: Equatable{
    
    
    public static func ==(lhs: Book, rhs: Book) -> Bool{
        
        return lhs.proxyForHash == rhs.proxyForHash
    }

    
}

extension Book: Comparable{
    
    public static func <(lhs: Book, rhs: Book) -> Bool{
        
        return lhs.proxyForHash < rhs.proxyForHash
    }

}
